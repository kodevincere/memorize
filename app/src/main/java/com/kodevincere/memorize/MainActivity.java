package com.kodevincere.memorize;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kodevincere.memorize.lib.Memorize;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("MemorizeLog", "INIT MEMORIZE");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Memorize.isSet("firstLog"))
            Log.d("MemorizeLog", Memorize.getString("firstLog"));
        else
            Memorize.put("MemorizeLog", "FIRST VALUE");
    }
}
