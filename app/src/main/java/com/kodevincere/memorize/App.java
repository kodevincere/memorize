package com.kodevincere.memorize;

import android.app.Application;

import com.kodevincere.memorize.lib.Memorize;

/**
 * Created by James
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Memorize.initialize()
                .context(this)
                .mode(MODE_PRIVATE)
                .prefsName("dsdsds")
                .build();
    }
}
