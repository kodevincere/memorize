package com.kodevincere.memorize.lib;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by James
 */
public class Memorize {

    private static final String DEFAULT_SUFFIX = "_memorize_manager_";
    private static boolean defaultBoolean = false;
    private static String defaultString = "default_memorize_value";
    private static int defaultInteger = -345214;
    private static long defaultLong = -345214;
    private static float defaultFloat = -345214;

    private static SharedPreferences prefs;
    private static ConcurrentMap<String, Object> mData;
    private static Memorize sInstance;

    private String mKey;
    private Context mContext;
    private int mMode = -1;
    private boolean mUseDefault = false;


    private Memorize(){}

    private static void init(Context context, String prefsName, int mode) {
        prefs = context.getSharedPreferences(prefsName, mode);
        mData = new ConcurrentHashMap<>();
        mData.putAll(prefs.getAll());
    }

    private static Memorize getInstance() {
        if (sInstance == null) {
            throw new RuntimeException("Memorize was not initialized! You must call Memorize.initialize() before using this.");
        }
        return sInstance;
    }

    public static Memorize initialize(){
        if(sInstance == null)
            sInstance = new Memorize();

        return sInstance;

    }

    public Memorize prefsName(final String prefsName) {
        mKey = prefsName;
        return this;
    }

    public Memorize context(final Context context) {
        mContext = context;
        return this;
    }

    public Memorize mode(final int mode) {
        if (mode == ContextWrapper.MODE_PRIVATE || mode == ContextWrapper.MODE_WORLD_READABLE || mode == ContextWrapper.MODE_WORLD_WRITEABLE || mode == ContextWrapper.MODE_MULTI_PROCESS) {
            mMode = mode;
        } else {
            throw new RuntimeException("The mode in the SharedPreference can only be set too ContextWrapper.MODE_PRIVATE, ContextWrapper.MODE_WORLD_READABLE, ContextWrapper.MODE_WORLD_WRITEABLE or ContextWrapper.MODE_MULTI_PROCESS");
        }

        return this;
    }

    public Memorize setUseDefaultSharedPreference(boolean defaultSharedPreference) {
        mUseDefault = defaultSharedPreference;
        return this;
    }

    public void build() {
        if (mContext == null) {
            throw new RuntimeException("Context not set, please set context before building");
        }

        if (TextUtils.isEmpty(mKey)) {
            mKey = mContext.getPackageName();
        }

        if (mUseDefault) {
            mKey += DEFAULT_SUFFIX;
        }

        if (mMode == -1) {
            mMode = ContextWrapper.MODE_PRIVATE;
        }

        init(mContext, mKey, mMode);
    }

    public static float getFloat(String key) {
        Float value = getInstance().get(key, Float.class);
        return value != null ? value : defaultFloat;
    }

    public static int getInt(String key) {
        Integer value = getInstance().get(key, Integer.class);
        return value != null ? value : defaultInteger;
    }

    public static long getLong(String key) {
        Long value = getInstance().get(key, Long.class);
        return value != null ? value : defaultLong;
    }

    public static String getString(String key) {
        String value = getInstance().get(key, String.class);
        return value != null ? value : defaultString;
    }

    public static boolean getBoolean(String key) {
        Boolean value = getInstance().get(key, Boolean.class);
        return value != null ? value : defaultBoolean;
    }

    private <T> T get(String key, Class<T> clazz) {
        Object value = mData.get(key);
        T castedObject = null;
        if (clazz.isInstance(value)) {
            castedObject = clazz.cast(value);
        }
        return castedObject;
    }

    public static boolean put(String key, Object value) {
        mData.put(key, value);
        return save(key, value);
    }

    private static boolean save(String key, Object value) {
        boolean success = false, put = true;
        SharedPreferences.Editor editor = prefs.edit();

        if (value instanceof Float) editor.putFloat(key, (Float) value);
        else if (value instanceof Integer) editor.putInt(key, (Integer) value);
        else if (value instanceof Long) editor.putLong(key, (Long) value);
        else if (value instanceof String) editor.putString(key, (String) value);
        else if (value instanceof Boolean) editor.putBoolean(key, (Boolean) value);
        else put = false;

        if (put) success = editor.commit();

        return success;
    }

    public static boolean isSet(String key) {
        return mData.containsKey(key);
    }

    public static void delete(String key) {
        mData.remove(key);
        prefs.edit().remove(key).apply();
    }

    public static void clear() {
        mData.clear();
        prefs.edit().clear().apply();
    }

}